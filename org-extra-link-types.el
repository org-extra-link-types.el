;;; org-extra-link-types.el --- Add extra link types to Org Mode -*- lexical-binding: t; -*-

;; Time-stamp: <2020-08-13 19:50:23 stardiviner>

;; Authors: stardiviner <numbchild@gmail.com>
;; Package-Requires: ((emacs "27.1") (olc "1.4.1"))
;; Package-Version: 0.1
;; Keywords: text org
;; homepage: https://repo.or.cz/org-extra-link-types.el.git

;; org-extra-link-types is free software; you can redistribute it and/or modify it
;; under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.
;;
;; org-extra-link-types is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
;; or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
;; License for more details.
;;
;;
;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; (use-package org-extra-link-types
;;   :vc (:url "git://repo.or.cz/org-extra-link-types.el.git")
;;   :demand t)

;;; Code:

(require 'cl-lib)
(require 'ol)
(require 'browse-url)

(defgroup org-extra-link-types nil
  "The customize group of package org-extra-link-types."
  :prefix "org-extra-link-types-"
  :group 'org)

;;===============================================================================
;;; [ telnet: ]

;;  e.g. telnet://ptt.cc
(org-link-set-parameters "telnet" :follow #'telnet)

;;===============================================================================
;;; [ chrome: ]

;;; e.g. chrome:chrome://whats-new/

(cl-case system-type
  (gnu/linux
   (cond
    ((executable-find "google-chrome")
     (org-link-set-parameters "chrome" :follow #'browse-url-chrome))
    ((executable-find "chromium")
     (org-link-set-parameters "chrome" :follow #'browse-url-chromium))))
  (darwin
   (cond
    ((file-exists-p "/Applications/Google Chrome.app")
     (org-link-set-parameters "chrome" :follow #'browse-url-chrome))
    ((file-exists-p "/Applications/Chromium.app")
     (org-link-set-parameters "chrome" :follow #'browse-url-chromium))))
  (windows-nt
   ;; TODO:
   ;; https://stackoverflow.com/questions/40674914/google-chrome-path-in-windows-10
   ))

;;===============================================================================
;;; [ rss: ]

(defun org-rss-link-open (uri)
  "Open rss:// URI link."
  (eww uri))

(org-link-set-parameters "rss" :follow #'org-rss-link-open)

;;===============================================================================
;;; [ tag: ]

;; e.g. [[tag:work+phonenumber-boss][Optional Description]]

(defun org-tag-link-open (tag)
  "Display a list of TODO headlines with tag TAG.
With prefix argument, also display headlines without a TODO keyword."
  (org-tags-view (null current-prefix-arg) tag))

(org-link-set-parameters "tag" :follow #'org-tag-link-open)

;;===============================================================================
;;; [ track: ]

;;; `track:' for OSM Maps
;; [[track:((9.707032442092896%2052.37033874553582)(9.711474180221558%2052.375238282987))data/images/org-osm-link.svg][Open this link will generate svg, png image for track link on map]]

(if (featurep 'org-osm-link)
    (require 'org-osm-link))

;;===============================================================================
;;; [ geo: ]

;; [geo:37.786971,-122.399677;u=35]
;; [[geo:58.397813,15.576063]]
;; [[geo:9FCQ9HXG+4CG]]

;;; Open Location Code library `olc'
(autoload 'olc-encode "olc")
(autoload 'olc-decode "olc")

(defcustom org-geo-link-application-command "gnome-maps"
  "Specify the program name for openning geo: link."
  :type 'string
  :group 'org-extra-link-types)

(defun org-geo-link-open (link)
  "Open geography `LINK' like \"geo:25.5889136,100.2208514\" in Map application."
  (if (featurep 'olc)
      (let ((location (cond
                       ;; (string-match-p "\\,.*" "25.5889136,100.2208514")
                       ((string-match-p "\\,.*" link)
                        link)
                       ;; (string-match-p "\\+.*" "9FCQ9HXG+4CG")
                       ((string-match-p "\\+.*" link)
                        (when (featurep 'olc)
                          (format "%s,%s"
                                  (olc-area-lat (olc-decode link))
                                  (olc-area-lon (olc-decode link)))))
                       (t (user-error "Your link is not Geo location or Open Location Code!")))))
        (if (executable-find org-geo-link-application-command)
            (start-process
             "org-geo-link-open"
             "*org-geo-link-open*"
             org-geo-link-application-command
             (shell-quote-wildcard-pattern location))
          (browse-url location)))
    (user-error "Emacs package 'olc' is not installed, please install it")))

(org-link-set-parameters "geo" :follow #'org-geo-link-open)

;;===============================================================================
;;; [ magnet: ]

;;; e.g. [[magnet:?xt=urn:btih:07327331BA1AD104828A6C07EAA48F6977D842AC]]

(defcustom org-magnet-link-open-command
  (cl-case system-type
    (darwin
     (cond
      ((file-exists-p "/Applications/Thunder.app")
       "open /Applications/Thunder.app")
      ((file-exists-p "/Applications/Motrix.app")
       "open /Applications/Motrix.app")
      ((file-exists-p "/Applications/qBittorrent.app")
       "open /Applications/qBittorrent.app")
      (t "open --url")))
    (gnu/linux "xdg-open")
    (windows-nt))
  "Specify the magnet: link open command."
  :type 'string
  :group 'org-extra-link-types)

(defun org-magnet-link-open (link)
  "Open magnet: LINK with downloader application."
  (let ((url (concat "magnet:" link)))
    (shell-command (concat org-magnet-link-open-command " " url))))

(org-link-set-parameters "magnet" :follow #'org-magnet-link-open)

;;===============================================================================
;;; [ ipfs:// ]

;; ipfs://<cid>/<path>
;; ipns://<ipns-name>/<path>
;;
;; ipfs://{cid}/path/to/subresource/cat.jpg
;;
;; Examples:
;;
;; ipfs://{cidv1}
;; ipfs://{cidv1}/path/to/resource
;; ipfs://{cidv1}/path/to/resource?query=foo#fragment
;;
;; ipns://{cidv1-libp2p-key}
;; ipns://{cidv1-libp2p-key}/path/to/resource
;; ipns://{dnslink-name}/path/to/resource?query=foo#fragment

(defun org-ipfs-link-open (url)
  "Open ipfs:// link URL with IPFS Desktop."
  (let ((url (shell-quote-argument url)))
    (async-shell-command
     (cl-case system-type
       (gnu/linux (format "open -a \"/Applications/IPFS Desktop.app\" --args %s" url))
       (darwin (format "ipfs --args %s" url))
       (windows-nt (format "ipfs --args %s" url))))))

(org-link-set-parameters "ipfs" :follow 'org-ipfs-link-open)

;;===============================================================================
;;; [ video: ]

;;; e.g. [[video:/path/to/file.mp4::00:13:20]]

(defcustom org-video-link-open-command
  (cond
   ((executable-find "mpv") "mpv")
   ((executable-find "mplayer") "mplayer")
   ((executable-find "iina") "iina"))
  "Specify the program for openning video: link."
  :type 'string
  :group 'org-extra-link-types)

(defvar org-video-link-extension-list '("avi" "rmvb" "ogg" "mp4" "mkv"))

(defun org-video-link-open (uri)
  "Open video file `URI' with video player."
  (let* ((list (split-string uri "::"))
         (path (car list))
         (start-timstamp (cadr list)))
    (pcase org-video-link-open-command
      ("mpv"
       (make-process
        :command (list "mpv" (concat "--start=" start-timstamp)
                       (expand-file-name (org-link-unescape path)))
        :name "org-video-link"
        :buffer " *org-video-link*"))
      ("mplayer"
       (make-process
        :command (list "mplayer" "-ss" start-timstamp
                       (expand-file-name (org-link-unescape path)))
        :name "org-video-link"
        :buffer " *org-video-link*"))
      ("iina"
       (make-process
        :command (list "iina" (concat "--mpv-start=" start-timstamp)
                       (expand-file-name (org-link-unescape path)))
        :name "org-video-link"
        :buffer " *org-video-link*")))))

(defun org-video-link-complete (&optional args)
  "Create a video link using completion with ARGS."
  (let ((file (read-file-name "Video: " nil nil nil nil
                              #'(lambda (file)
                                  (seq-contains-p
                                   org-video-link-extension-list
                                   (file-name-extension file)))))
        (pwd (file-name-as-directory (expand-file-name ".")))
        (pwd1 (file-name-as-directory (abbreviate-file-name
                                       (expand-file-name ".")))))
    (cond ((equal args '(16))
           (concat "video:"
                   (abbreviate-file-name (expand-file-name file))))
          ((string-match
            (concat "^" (regexp-quote pwd1) "\\(.+\\)") file)
           (concat "video:" (match-string 1 file)))
          ((string-match
            (concat "^" (regexp-quote pwd) "\\(.+\\)")
            (expand-file-name file))
           (concat "video:"
                   (match-string 1 (expand-file-name file))))
          (t (concat "video:" file)))))

(org-link-set-parameters
 "video"
 :follow #'org-video-link-open
 :complete #'org-video-link-complete)

;;===================================================================================================
;;; [ web-browser: ] -- open Org link with web browser.

(defun org-web-browser--insert-description (link description)
  (string-trim-left link "web-browser:"))

(defun org-web-browser--complete-link ()
  "Create a file link using completion."
  (let ((file (read-file-name "Local HTML File: ")))
    (concat "web-browser:" file)))

(defun org-web-browser--store-link ()
  "Store a link from a local HTML file."
  (when (member major-mode '(html-mode html-ts-mode html-erb-mode web-mode))
    (let* ((link (buffer-file-name (current-buffer)))
           (description (file-name-nondirectory link)))
      (org-link-store-props
       :type "web-browser"
       :link link
       :description description))))

(defun org-web-browser--open-link (path &optional _)
  "Open local HTML file in PATH with web browser through function `browse-url'."
  (browse-url path))

(defun org-web-browser--export-link (link &optional description backend)
  "Export a local HTML file LINK with DESCRIPTION.
BACKEND is the current export backend."
  (org-export-file-uri link))

(org-link-set-parameters
 "web-browser"
 :insert-description #'org-web-browser--insert-description
 :complete           #'org-web-browser--complete-link
 :store              #'org-web-browser--store-link
 :follow             #'org-web-browser--open-link
 :export             #'org-web-browser--export-link)

;;===================================================================================================
;;; [ javascript: / js: ] -- Org link for executing simple JavaScript code.

;;; e.g. [[javascript:console.log("hello, world");]]

(defun org-javascript--insert-description (link &optional description)
  (string-trim-left link "javascript:"))

(defun org-javascript--enable-code-completion ()
  "Enable Corfu in the minibuffer if `completion-at-point' is bound."
  (setq-local corfu-auto t)
  (setq-local corfu-auto-prefix 1)
  (setq-local completion-at-point-functions
              (list #'cape-file
                    #'cape-keyword
                    (cape-company-to-capf 'company-yasnippet)
                    #'cape-history
                    #'cape-abbrev #'cape-dabbrev))
  (corfu-mode 1))

(defmacro org-javascript--enable-code-completion-wrapper (&rest body)
  "Use a wrapper macro to add & clean `string-edit-mode-hook' without affecting global environment."
  `(let ((link))
     (add-hook 'string-edit-mode-hook #'org-javascript--enable-code-completion)
     (setq link ,@body)
     (remove-hook 'string-edit-mode-hook #'org-javascript--enable-code-completion)
     ;; The `link' is the JS code as variable of eval result pass to `org-insert-link'.
     link))

(defun org-javascript--complete-code (&optional _arg)
  "Complete JavaScript code in javascript: link."
  (org-javascript--enable-code-completion-wrapper
   (substring-no-properties
    (read-string-from-buffer            ; wrapper of `string-edit' with `callback'.
     "Write Org javascript: link code here: "
     "console.log(\"hello, world\");"))))

(defun org-javascript--store-link ()
  "Store link from JavaScript code."
  (when (member major-mode '(js-mode js2-mode javascript-mode))
    (unless (or (region-active-p) (use-region-p))
      (mark-paragraph))
    (let* ((org-store-props-function
            (if (fboundp 'org-link-store-props)
                'org-link-store-props
              'org-store-link-props))
           (code (replace-regexp-in-string
                  "\n" " " ; replace newline with space to avoid multiple line code issue in link.
                  (buffer-substring-no-properties (region-beginning) (region-end))))
           (link (format "javascript:%s" code))
           (description (concat "JavaScript code: " (s-truncate (- (window-width) 40) code))))
      (deactivate-mark)
      (funcall org-store-props-function
               :type "javascript"
               :link link
               :description description))))

(defun org-javascript--execute-code (code &optional _)
  "Execute the JavaScript CODE in javascript: link."
  (cond
   ((featurep 'indium)
    (indium-launch)
    (indium-eval code (lambda (result) result)))
   ((featurep 'skewer-mode)
    (skewer-repl)
    (skewer-eval code (lambda (result) result)))
   ((featurep 'js-comint)
    (js-comint-start-or-switch-to-repl)
    (js-comint-send-string code))))

(defun org-javascript--export-code (link description backend)
  "Export a javascript: / js: LINK code with DESCRIPTION.
BACKEND is the current export backend."
  ;; Evaluate javascript code in link, display eval result output in exporting.
  (org-javascript--execute-code link))

(org-link-set-parameters
 "javascript"
 :insert-description #'org-javascript--insert-description
 :complete           #'org-javascript--complete-code
 :store              #'org-javascript--store-link
 :follow             #'org-javascript--execute-code
 :export             #'org-javascript--export-code)

(org-link-set-parameters
 "js"
 :insert-description #'org-javascript--insert-description
 :complete           #'org-javascript--complete-code
 :store              #'org-javascript--store-link
 :follow             #'org-javascript--execute-code
 :export             #'org-javascript--export-code)

;;===================================================================================================
;;; Add "vscode:" link type to open link in Visual Studio Code.

(defun org-vscode-open (path &optional _)
  "Open link PATH with Visual Studio Code."
  (if (string-match-p "//settings/.*" path)
      ;; [[vscode://settings/terminal.integrated.shellIntegration.enabled]]
      (cl-case system-type
        (darwin
         (start-process "org-vscode-open" " *org-vscode-open*" "open" (concat "vscode:" path)))
        (gnu/linux
         (start-process "org-vscode-open" " *org-vscode-open*" "xdg-open" (concat "vscode:" path))))
    ;; $ code /path/to/file_or_directory
    (start-process
     "org-vscode-open"
     "*org-vscode-open*"
     "code" (expand-file-name path)))
  (message "%s Visual Studio Code opened Org linked project -> \"%s\""
           (nerd-icons-mdicon "nf-md-microsoft_visual_studio_code" :face 'nerd-icons-blue-alt)
           path))

(defun org-vscode-complete (&optional _arg)
  "Complete path for vscode: link."
  (let ((path (expand-file-name
               (substring-no-properties
                (read-file-name "Visual Studio Code link 'vscode:' path: ")))))
    (concat "vscode:" path)))

(org-link-set-parameters
 "vscode"
 :complete           #'org-vscode-complete
 :follow             #'org-vscode-open)

;;===================================================================================================
;;; Add "weixin:" link type to open link in WeChat.

;; https://stackoverflow.com/questions/35425553/how-do-i-link-to-wechat-from-a-webpage
;; WeChat does have a URI scheme that can be used from a browser. The scheme prefix is `weixin://'.
;;
;; There are a few URIs that can be used with this:
;;
;; `weixin://dl/stickers'
;; `weixin://dl/settings'
;; `weixin://dl/posts'
;; `weixin://dl/moments'
;; However, in answer to your question specifically, there is one where you can chat to a contact specifically:
;;
;; `weixin://dl/chat?{toID}'
;;
;; You will need to replace `{toID}' with whatever the destination user's WeChat ID is.

(defun org-weixin-link-open (path &optional _args)
  "Open WeiXin link PATH with optional ARGS."
  (cl-case system-type
    (darwin
     ;; open or switch to WeChat Application at first to make it available.
     (ns-do-applescript
"tell application \"System Events\" to tell process \"WeChat\"
    if visible is true then
        set visible to true
    else
        tell application \"WeChat\" to activate
    end if
end tell")
     (start-process "org-weixin-link-open"
                    " *org-weixin-link-open*"
                    "open" (concat "weixin:" path)))
    (gnu/linux
     (start-process "org-weixin-link-open"
                    " *org-weixin-link-open*"
                    "xg-open" (concat "weixin:" path)))))

(dolist (link-type '("weixin" "wechat"))
  (org-link-set-parameters
   link-type
   :follow   #'org-weixin-link-open))

;;===================================================================================================
;;; Add "macappstore:" link type to open link in macOS App Store.

;; This works with the new Mac App Store on Mojave.
;; macappstore://apps.apple.com/app/idxxxxxxxxx?action=write-review
;; If you also want to support iOS as well, use this general link:
;; https://apps.apple.com/app/idxxxxxxxxx?action=write-review
;; Replace xxxxxxxxx with your App ID. (can be found on App Store Connect)

(defun org-macappstore-open (url &optional _args)
  "Open macappstore:// link URL with ARGS from `browse-url'."
  (when (eq system-type 'darwin)
    ;; $ open vscode://settings/terminal.integrated.shellIntegration.enabled
    (start-process "org-macappstore-link-open"
                   " *org-macappstore-link-open*"
                   "open" (concat "macappstore:" url))))

(org-link-set-parameters
 "macappstore"
 :follow #'org-macappstore-open)

;;; Add "applescript:" link type to open AppleScript Editor on macOS.

;; "applescript://com.apple.scripteditor?action=new&script=..."

;; e.g. applescript://com.apple.scripteditor?action=new&script=%0Atell%20application%20%22System%20Events%0A
;; (url-hexify-string "tell application \"System Events\"")
;; (url-hexify-string "\n")

(defun org-applescript-link-store ()
  "Store applescript:// link in `applescript-mode'."
  (when (or (eq major-mode 'applescript-mode)
            (eq major-mode 'apples-mode))
    (let ((link (concat "applescript://com.apple.scripteditor?action=new&script="
                       (url-hexify-string
                        (concat "\n" (buffer-substring-no-properties (point-min) (point-max)) "\n"))))
          (description "applescript: code link open AppleScript Editor"))
      (org-link-store-props
       :type "applescript"
       :link link
       :description description))))

(defun org-applescript-link-open (url &optional _args)
  "Open applescript:// link URL with ARGS using AppleScript Editor on macOS."
  (when (eq system-type 'darwin)
    (start-process "org-applescript-link-open"
                   " *org-applescript-link-open*"
                   "open" (concat "applescript:" url))))

(org-link-set-parameters
 "applescript"
 :store #'org-applescript-link-store
 :follow #'org-applescript-link-open)



(provide 'org-extra-link-types)

;;; org-extra-link-types.el ends here
